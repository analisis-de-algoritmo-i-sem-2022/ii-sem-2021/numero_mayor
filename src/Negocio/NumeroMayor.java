/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author madar
 */
public class NumeroMayor {

    private short vectorNumero[];

    public NumeroMayor() {
    }

    /**
     * Recibe un n positivo Ejemplo: n=56890 Vector: {5,6,8,9,0}
     *
     * @param n entero positivo
     */
    public NumeroMayor(long n) {
        // Precondiciòn
        if (n <= 0) {
            throw new RuntimeException("El número debe ser mayor que 0");
        }
        //Cuál es el tamaño del vector?

        int con = 0;

        //Cúal es el tamaño máximo del vector? --> Peor caso:
        // tamaño máximo del vector es el X dígitos (donde X es el número màx de cifras de un long)
        long aux = n;
        while (n != 0) {
            n = n / 10;
            con++;

        }
        this.vectorNumero = new short[con];
        for (int i = con - 1; i >= 0; i--) {
            this.vectorNumero[i] = (short) (aux % 10);
            aux /= 10;
        }
    }

    /**
     * Constructor con algunas mejoras
     *
     * @param n numero long
     * @param sw cualquier valor T/F
     */
    public NumeroMayor(long n, boolean sw) {
        // Precondiciòn
        if (n <= 0) {
            throw new RuntimeException("El número debe ser mayor que 0");
        }
        //Cuál es el tamaño del vector?

        int con = 0;

        //Cúal es el tamaño máximo del vector? --> Peor caso:
        // tamaño máximo del vector es el X dígitos (donde X es el número màx de cifras de un long)
        long aux = n;
        //Mejora simple:(log10n)
        con = (int) Math.log10(n) + 1;
        this.vectorNumero = new short[con];
        for (int i = con - 1; i >= 0; i--) {
            this.vectorNumero[i] = (short) (aux % 10);
            aux /= 10;
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (short dato : this.vectorNumero) {
            msg += dato + "\t";
        }
        return msg;
    }

    /**
     * A partir de:
     * https://procomsys.wordpress.com/2018/06/25/caso-practico-en-java-ordenamiento-de-burbuja-bubble-sort/
     */
    private void ordenaBurbuja() {
        short ArrayN[] = this.vectorNumero;
        for (int i = 0; i < ArrayN.length - 1; i++) {
            /* Bucle anidado desde 0 hasta la longitud del array -1 */
            for (int j = 0; j < ArrayN.length - 1; j++) {
                /* Si el número almacenado en la posición j es mayor que el de la posición j+1 (el siguiente del array) */
                if (ArrayN[j] < ArrayN[j + 1]) {
                    /* guardamos el número de la posicion j+1 en una variable (el menor) */
                    short temp = ArrayN[j + 1];
                    /* Lo intercambiamos de posición */
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                    /* y volvemos al inicio para comparar los siguientes hasta que todos se hayan comparado*/
 /* de esta forma vamos dejando los números mayores al final del array en orden*/
                }

            }

        }
    }

    public long getNumeroMayor() {
        if (this.vectorNumero == null) {
            throw new RuntimeException("No se puede encontrar el número");
        }
        //Opcional--> Poco eficiente--> Si eficaz, posiblemente con CORRECTITUD:
        long num = 0;
        this.ordenaBurbuja();
        for (int i = 0; i < vectorNumero.length; i++) {
            num = num * 10 + this.vectorNumero[i];

        }
        return num;
    }

}
